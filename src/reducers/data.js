const dataInitialState = {
    data: []
  }
  
  const dataReducer = (state = dataInitialState, action) => {
    const newState = { ...state }  
      switch (action.type) {
        case 'TASKS_DATA':
            localStorage.setItem('tasks',JSON.stringify(action.data.lanes));
            return {
                data: action.data.lanes,
            }
      default:
        return newState
    }
  }
  
  export default dataReducer
  