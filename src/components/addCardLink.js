import React from 'react'
import { Button } from 'antd';


const CustomAddCardLink =  ({onClick, t}) => <Button className="add-card-button" type="primary" block onClick={onClick}>Add New Task</Button>

export default CustomAddCardLink