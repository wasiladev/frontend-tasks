
import 'antd/dist/antd.css';
import { notification } from 'antd';
import Store from '../store.js'

const OpenNotification = (id,type) => {
  let cardId = id
  let store = Store.getState();
  let tasksData = store.data.data
  let currentCard
  tasksData.find((task) => {
    // eslint-disable-next-line array-callback-return
    return task.cards.some((item) => {
      if(item.id === cardId){
        currentCard = item
        return currentCard
      }
    });
  });

  if(type === "delete"){
    notification.open({
      message: `Task ${currentCard.title} Deleted`,
      description:`${currentCard.description}`,
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
  } else if(type === "add"){
    notification.open({
      message: `Task ${currentCard.title} Added`,
      description:`${currentCard.description}`,
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
  } else if(type === "cancel"){
    notification.open({
      message: `You Have Already Completed Task ${currentCard.title} You Can't Move It`,
      description:`${currentCard.description}`,
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
  }


};

export default OpenNotification