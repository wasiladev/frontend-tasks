import React, { Component } from "react";
import { PageHeader, Descriptions } from 'antd';

export default class AppHeader extends Component {
  render() {
    return (
 <div className="site-page-header-ghost-wrapper main-app-header">
    <PageHeader
      ghost={false}
      title="Task Management Board Application"
    >    
      <Descriptions size="small" column={3}>
        <Descriptions.Item label="Created By">Ahmed Essam Mohammed Ali</Descriptions.Item>
        <Descriptions.Item label="Created For">Buseet Senior Front-end Engineer Task</Descriptions.Item>
        <Descriptions.Item label="">
          <a href="https://drive.google.com/file/d/1zIQgDzNnoM2XNIxE_GAyV6c5kFRvl_1c/view?usp=sharing" target="_blank" rel="noreferrer">My Resume</a>
        </Descriptions.Item>
      </Descriptions>
    </PageHeader>
  </div>
    );
  }
}
