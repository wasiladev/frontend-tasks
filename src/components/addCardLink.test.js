import React from 'react';
import {render} from '@testing-library/react';
import CustomAddCardLink from './addCardLink';
import {configure, shallow, mount} from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });


global.matchMedia = global.matchMedia || function () {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
};

const setUp = () => {
    const wrapper = mount(<CustomAddCardLink/>);
    return wrapper;
};


describe('Button Testing', () => { 
    let wrapper = setUp()
    let myButton = wrapper.find('Button');


    test('Test My Button Exist', () => { 
        expect(myButton);
    })    
    test('Test My Button type', () => { 
        expect(myButton.props()["type"]).toBe('primary');
    })    
    test('Test My Button block', () => { 
        expect(myButton.props()["block"]);
    })  
    test('Test My Button Text', () => { 
        expect(myButton.text()).toEqual('Add New Task');
    })      


})


