import React, { Component } from 'react'
import { Button } from 'antd';

class NewCardForm extends Component {
  handleAdd = () => this.props.onAdd({title: this.titleRef.value, description: this.descRef.value});
  setTitleRef = (ref) => this.titleRef = ref
  setDescRef = (ref) => this.descRef = ref

  constructor() {
    super();
    this.state = {
      title: '',
      description: '',
    };
  }

  handleTitleChange = (evt) => {
    this.setState({ title: evt.target.value });
  }
  
  handleDescriptionChange = (evt) => {
    this.setState({ description: evt.target.value });
  }

  render() {
    const {onCancel} = this.props
    const { title, description } = this.state;
    return (
      <div style={{background: 'white', borderRadius: 3, border: '1px solid #eee', borderBottom: '1px solid #ccc'}}>
        <div style={{padding: 5, margin: 5}}>
          <div>
            <div style={{marginBottom: 5}}>
              <input className="add-task-input" type="text"  placeholder="Task Title" ref={this.setTitleRef}  value={this.state.title}
          onChange={this.handleTitleChange}/>
            </div>
            <div style={{marginBottom: 5}}>
              <input className="add-task-input" type="text" placeholder="Task Description" ref={this.setDescRef} value={this.state.description}
          onChange={this.handleDescriptionChange}/>
            </div>
          </div>

          <div className="add-task-action">
            <Button className="submit-form-button" disabled={!title || !description} type="primary" onClick={this.handleAdd}>Add Task</Button>
            <Button className="cancel-form-button" type="primary" danger onClick={onCancel}>Cancel</Button>
          </div>



        </div>
      </div>



    )
  }
}




export default NewCardForm
