import React, { Component } from "react";
import { Layout } from 'antd';
const { Footer } = Layout;

export default class AppFooter extends Component {
  render() {
    return (
    <Footer style={{ textAlign: 'center' }}>Task Management Board Application ©2021 Created by Ahmed Essam</Footer>
    );
  }
}
