import React from 'react';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App';
import { Provider } from 'react-redux';
import {configure, shallow, mount} from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { findByTestAtrr, testStore } from './Utils/index';

configure({ adapter: new Adapter() });


global.matchMedia = global.matchMedia || function () {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
};

const setUp = (initialState={}) => {
    const store = testStore(initialState);
    const wrapper = mount(<App store={store} />);
    return wrapper;
};


describe('Test the board', () => { 
    let wrapper = setUp();
    const mockCallBack = jest.fn();
    test('render 3 boards (To Do, In Progress, Done)', () => { 
        expect(wrapper.find('Draggable').get(0));
        expect(wrapper.find('Draggable').get(1));
        expect(wrapper.find('Draggable').get(2));
    });

    test('render Add a card button to the board', () => { 
        expect(wrapper.find('CustomAddCardLink').get(0));
        expect(wrapper.find('CustomAddCardLink').get(1));
        expect(wrapper.find('CustomAddCardLink').get(2));
    })  


    test('render Add a card button to the board', () => { 
        wrapper.find('.add-card-button').first().simulate('click');
    })  


    test('render delete a card button to the board', () => { 
        expect(wrapper.find('DeleteButton'));
    }) 

})
