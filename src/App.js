import React, { useState } from 'react';
import Board from 'react-trello'
import { connect } from 'react-redux'
import tasksData from './json/data'
import NewCardForm from './components/addNewCard'
import CustomAddCardLink from './components/addCardLink'
import OpenNotification from './components/notification'
import AppHeader from './components/header'
import AppFooter from './components/footer'
import { Modal } from 'antd';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './App.scss';

function App(props) {
  let savedData = localStorage.getItem('tasks');
  let savedTaskes = `{"lanes":${savedData}}`;
  let currentTasks
  if (JSON.parse(savedTaskes).lanes != null){
    currentTasks = JSON.parse(savedTaskes);
  } else {
    currentTasks = tasksData;
  }
  const [data, setTasksData] = useState(currentTasks);
  const [modal, setModal] = useState(false);


  const handleOk = e => {setModal(false)};
  const handleCancel = e => {setModal(false)};

  const handleDragStart = (cardId, laneId) => {}
  const handleDragEnd = (cardId, sourceLaneId, targetLaneId, position, card) => {
    if(targetLaneId !== 'lane3' && sourceLaneId === "lane3"){
      OpenNotification(cardId,"cancel");
      return false
    }
  }
  const handleLaneDragStart = laneId => {}
  const handleLaneDragEnd = (removedIndex, addedIndex, {id}) => {}
  const shouldReceiveNewData = nextData => {
    setTasksData(nextData)
    props.updateTasksData(nextData);
  }
  const onCardMoveAcrossLanes = (fromLaneId, toLaneId, cardId, addedIndex) => {}
  const handleCardDelete = (cardId, laneId) => {
    OpenNotification(cardId,"delete");
  }
  const handleCardAdded = (card, laneId) => {
    setTimeout(() => {  
      OpenNotification(card.id,"add"); 
    }, 1000);
    
  }
  const handleCardClick = (cardId, metadata, laneId) => {}
  return (
    <div className="App">
      <header className="App-header">
        <AppHeader></AppHeader>
        <Board data={data} 
          draggable
          onCardMoveAcrossLanes={onCardMoveAcrossLanes}
          onDataChange={shouldReceiveNewData}
          handleDragStart={handleDragStart}
          handleDragEnd={handleDragEnd}
          handleLaneDragStart={handleLaneDragStart}
          handleLaneDragEnd={handleLaneDragEnd}
          editable
          onCardClick={handleCardClick}
          onCardAdd={handleCardAdded}
          onCardDelete={handleCardDelete}
          components={{
            NewCardForm: NewCardForm,
            AddCardLink: CustomAddCardLink
          }}
         />
         <Modal
          title="Basic Modal"
          visible={modal}
          onOk={handleOk}
          onCancel={handleCancel}
        ></Modal>
        <AppFooter></AppFooter>
      </header>
    </div>
  );
}


const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    updateTasksData: (data, index) => {
      dispatch({ type: 'TASKS_DATA', index, data })
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
