import {createStore, compose, applyMiddleware} from 'redux';
import promise from 'redux-promise';
import reduxThunk from 'redux-thunk';
import reducers from './reducers'

export const middlewares = [reduxThunk];

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const Store = createStore(reducers, composeEnhancer(applyMiddleware(promise, reduxThunk)))

export default Store