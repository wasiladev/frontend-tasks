const tasksData = {
    lanes: [
      {
        id: 'lane1',
        title: 'Todo Tasks',
        disallowAddingCard: false,
        cards: [
          {id: 'Card1', title: 'HTML', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card2', title: 'CSS', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card3', title: 'JavaScript', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card4', title: 'Angular', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}}
        ]
      },
      {
        id: 'lane2',
        title: 'In Progress Tasks',
        disallowAddingCard: false,
        cards: [
          {id: 'Card5', title: 'React', description: 'Lorem ipsum dolor sit amet', label: '5 mins', draggable: true, metadata: {sha: 'be312a1'}},
          {id: 'Card6', title: 'Vue', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card7', title: 'Ember.js', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card8', title: 'Node.js', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}}
        ]
      },
      {
        id: 'lane3',
        title: 'Completed Tasks',
        disallowAddingCard: false,
        cardColor: '#ccff90',
        cards: [
          {id: 'Card9', title: 'Polymer', description: 'Lorem ipsum dolor sit amet', label: '5 mins', draggable: true, metadata: {sha: 'be312a1'}},
          {id: 'Card10', title: 'Backbone.js', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card11', title: 'Git and Version Control', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}},
          {id: 'Card12', title: 'Problem Solving', description: 'Lorem ipsum dolor sit amet', label: '5 mins', metadata: {sha: 'be312a1'}}
        ]
      }
    ]
  }



  export default tasksData;